# schema-validator



## Installation

1. Get node-js (>= 16.0.0)
2. Get npm 
3. Install dependencies run `npm install`

## Example

### Validation OK

```bash
# SHACL validation OK
npm run demo-ok
```

This returns exit code 0.
```bash
npm run demo-ok

> schema-validator@1.0.0 demo-ok
> node test.js ./SHACL/providerShape.ttl ./data/provider-ok.json

Validate data graph ./data/provider-ok.json using shape graph ./SHACL/providerShape.ttl
Conformity :true
```

### Validation KO

```bash
# SHACL validation KO:
# Missing required hasDidWeb
# Wrong type for one hasServiceOffering
npm run demo-ko
```

```bash
npm run demo-ko

> schema-validator@1.0.0 demo-ko
> node test.js ./SHACL/providerShape.ttl ./data/provider-ko.json

Validate data graph ./data/provider-ko.json using shape graph ./SHACL/providerShape.ttl
Conformity :false


Print report results...
Message 
Path https://schemas.abc-federation.gxfs.fr/wip/vocab/participant#hasServiceOffering
Focus node [object Object]
Severity http://www.w3.org/ns/shacl#Violation
Source constraint http://www.w3.org/ns/shacl#ClassConstraintComponent
Source shape _:b2


Print report results...
Message Less than 1 values
Path https://schemas.abc-federation.gxfs.fr/wip/vocab/participant#hasDidWeb
Focus node [object Object]
Severity http://www.w3.org/ns/shacl#Violation
Source constraint http://www.w3.org/ns/shacl#MinCountConstraintComponent
Source shape _:b3
```
