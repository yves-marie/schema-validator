import express from 'express';
import fs from 'fs';
import factory from 'rdf-ext';
import ParserN3 from '@rdfjs/parser-n3';
import ParserJsonld from '@rdfjs/parser-jsonld'
import SHACLValidator from 'rdf-validate-shacl';
import { Console } from 'console';
import Readable from 'stream' 
import request from 'request';

const app = express()
const port = 8080;


app.get('/', (req, res) => {
  res.send('false')
})

async function loadDataset (filePath) {
  const stream = fs.createReadStream(filePath)
  const parser = new ParserN3({ factory })
  return factory.dataset().import(parser.import(stream))
}

let shapesFiles = new Map(); 

let listShapeFiles = new Map () ;
function init () {
  listShapeFiles.set ("participant", "https://schemas.abc-federation.gxfs.fr/22.04/validation/participantShape.ttl" );
  listShapeFiles.set ("serviceOffering", "https://schemas.abc-federation.gxfs.fr/22.04/validation/service-offeringShape.ttl");
}

function loadShapes () {
  listShapeFiles.forEach((value,key)=>{
    request(value,  {json: false}, (error, res, body) => {
      if (error) {
          console.log ("critical error while loading " + shape);
          process.exit(1);
      };
      if (!error && res.statusCode == 200) {
      console.log ("add entry shape" + key );
      shapesFiles.set (key,body);
      };
  } )
  })
}

async function loadJSONLDDataset (json) {
  const parserJsonld = new ParserJsonld()
  var s = new Readable.Readable()
  s.push(json)   
  s.push(null)     
 const output = parserJsonld.import( s);
return factory.dataset().import(output);
}

async function  check (shape, json) {
  //const shapes = await loadDataset('./SHACL/' +  shape);
 const shapes = shapesFiles.get(shape);
  const data = await loadJSONLDDataset(json);
  console.log (data);
  const validator = new SHACLValidator(shapes, { factory })
  const report = await validator.validate(data)
  console.log('--------');
  console.log ('return '+ report.conforms);
  console.log('--------');
  for (const result of report.results) {
    console.log(result.message)
    console.log(result.path)
    console.log(result.focusNode)
    console.log(result.severity)
    console.log(result.sourceConstraintComponent)
    console.log(result.sourceShape)
  }
return report.conforms;
}

 app.post('/:shapeId', (req, res) => {
  //console.log (req.body);
  check (req.params.shapeId,  req.body).then (report => {
    res.send (report);
    res.end();
  })
})

app.listen(port, () => {
  init();
  loadShapes ();
  console.log(`Example app listening on port ${port}`)
})
